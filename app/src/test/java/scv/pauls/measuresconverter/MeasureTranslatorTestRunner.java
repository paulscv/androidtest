package scv.pauls.measuresconverter;

import android.database.sqlite.SQLiteDatabase;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import scv.pauls.measuresconverter.data.MeasureKoeffSqlite;
import scv.pauls.measuresconverter.data.MeasureNameSqlite;
import scv.pauls.measuresconverter.data.MeasureSystemSqlite;
import scv.pauls.measuresconverter.data.MeasureTypeSqlite;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 *
 */

//@RunWith(MockitoJUnitRunner.class)

@RunWith(RobolectricTestRunner.class)
@Config(manifest=Config.NONE)
public class MeasureTranslatorTestRunner extends TestCase{

  //  @Mock
 //   private static Context mMockContext;


    // static Context mMockContext;
    // путь к базе данных вашего приложения
    private static String DB_PATH = "/data/";
    private static String DB_NAME = "C:\\Android\\Projects\\MeasuresConverter\\app\\src\\test\\res\\test_measures.db";
    private static SQLiteDatabase testDataBase = null;
    private static final int DATABASE_VERSION = 1;

    private static String dbPath;
    private static SQLiteDatabase dbConn;


    @Before
    public  void openTestDb() {
        try {
           // System.out.println( MeasureTranslatorTestRunner.class.getClassLoader().toString());
            List<URL> resList =  Collections.list( MeasureTranslatorTestRunner.class.getClassLoader().getResources(".")); //getResource(DB_PATH+DB_NAME).toURI().getPath();
            if (resList != null){
                System.out.println(resList.size());
                for (URL u : resList)  System.out.println(u.toString());}
            else System.out.println("ooops");
            // assertNotNull("Connection path emty", path);
            File dbFile = new File(DB_NAME);
            assertTrue("File not found!!!", dbFile.exists());
            dbPath = dbFile.getAbsolutePath();
            dbConn = SQLiteDatabase.openDatabase(dbPath, null,  SQLiteDatabase.OPEN_READONLY);
            assertNotNull("Connection not created!!!", dbConn);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    @Test
    public void testMeasureSystems() {
        Assert.assertNotNull("Connection empty", dbConn);
        MeasureSystemSqlite measureSystems = new MeasureSystemSqlite(dbConn);
        Assert.assertNotNull("Cann't get Measure Systems from db", measureSystems);
        MeasureSystem metricSystem = measureSystems.getMeasureSystem("METRIC");
        Assert.assertEquals("Cann't get correct measure system", "METRIC", metricSystem.getMeasureSystemCode());
    }

    @Test
    public void testMeasures() {
        Assert.assertNotNull("Connection empty", dbConn);
        MeasureTypeSqlite measures = new MeasureTypeSqlite(dbConn);
        Assert.assertNotNull("Cann't get Measures from db", measures);
        MeasureType mLength = measures.getMeasure("LENGTH");
        Assert.assertEquals("Cann't get correct measure", "LENGTH", mLength.getMeasureCode());
    }

    @Test
    public void testMeasureNames() {
        Assert.assertNotNull("Connection empty", dbConn);
        MeasureNameSqlite measureNames = new MeasureNameSqlite(dbConn);
        Assert.assertNotNull("Cann't get Measures Names from db", measureNames);
        MeasureName mName = measureNames.getMeasureName("METR");
        Assert.assertEquals("Cann't get correct measure name ", "METR", mName.getCode());
        Assert.assertEquals("Cann't get correct measure name system", "METRIC", mName.getMeasureSystemCode());
        Assert.assertEquals("Cann't get correct measure ", "LENGTH", mName.getMeasureCode());
    }

    @Test
    public void testgetKeof(){
        Assert.assertNotNull("Connection empty", dbConn);
        MeasureKoeffSqlite mKoefDb = new MeasureKoeffSqlite(dbConn);
        Assert.assertNotNull("Cann't get Measures Koef from db", mKoefDb);
        String codeFrom = "METR";
        String codeTo = "METR";
        MeasureKoeff mKoef= mKoefDb.getKoeff(codeFrom, codeTo);
        Assert.assertNotNull("Cann't get Koeff db", mKoef);
        int koef = mKoef.getKoeff().intValue() ;
        Assert.assertEquals("Cann't get correct measure", 1, koef);
    }

    @Test
    public void testConvertion(){
        Assert.assertNotNull("Connection empty", dbConn);
        MeasureKoeffSqlite mKoefDb = new MeasureKoeffSqlite(dbConn);
        Assert.assertNotNull("Cann't get Measures Koef from db", mKoefDb);
        String codeFrom = "INCH";
        String codeTo = "SANTIMETR";
        MeasureKoeff mKoef= mKoefDb.getKoeff(codeFrom, codeTo);
        Assert.assertNotNull("Cann't get Koeff db", mKoef);
        BigDecimal koef = mKoef.getKoeff();
        BigDecimal bdFrom = new BigDecimal("100");
        BigDecimal check = new BigDecimal("254");
        BigDecimal convRes = bdFrom.multiply(koef);
        assertEquals("Cann't get correct measure", 0, convRes.compareTo(check));
    }

    @After
    public void testDBConnectionClosing() {
        dbConn.close();
        assertFalse("DB Still opened",dbConn.isOpen() );
    }


}
