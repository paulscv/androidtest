package scv.pauls.measuresconverter;

/**
 * Created by Pauls on 12.01.2017.
 */

public class MeasureName {
    private String measureSystemCode;
    private String measureCode;
    private String code;
    private String codeDescr;
    private boolean isInSystem;
    private boolean isBaseMeasure;


    public MeasureName(String measureSystemCode, String measureCode, String code, String codeDescr) {
        super();
        this.measureSystemCode = measureSystemCode;
        this.measureCode = measureCode;
        this.code = code;
        this.codeDescr = codeDescr;
        this.isInSystem = true;
        this.isBaseMeasure = false;
    }

    public String getMeasureSystemCode() {
        return measureSystemCode;
    }
    public void setMeasureSystemCode(String measureSystemCode) {
        this.measureSystemCode = measureSystemCode;
    }
    public String getMeasureCode() {
        return measureCode;
    }
    public void setMeasureCode(String measureCode) {
        this.measureCode = measureCode;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getCodeDescr() {
        return codeDescr;
    }
    public void setCodeDescr(String codeDescr) {
        this.codeDescr = codeDescr;
    }
    public boolean isInSystem() {
        return isInSystem;
    }
    public void setInSystem(boolean isInSystem) {
        this.isInSystem = isInSystem;
    }
    public boolean isBaseMeasure() {
        return isBaseMeasure;
    }
    public void setIsBaseMeasure(boolean isBaseMeasure) {
        this.isBaseMeasure = isBaseMeasure;
    }

    @Override
    public String toString() {
        return "MeasureName [measureSystemCode=" + measureSystemCode + ", measureCode=" + measureCode + ", code=" + code
                + ", codeDescr=" + codeDescr + ", isInSystem=" + isInSystem + ", isBaseMeasure=" + isBaseMeasure + "]";
    }

}
