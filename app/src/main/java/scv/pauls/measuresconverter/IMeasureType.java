package scv.pauls.measuresconverter;


import java.util.ArrayList;

public interface IMeasureType {
	public MeasureType getMeasure(String measureCode);
	public ArrayList<MeasureType> getAllMeasures();
	public ArrayList<String> getMeasureNames();	
}
