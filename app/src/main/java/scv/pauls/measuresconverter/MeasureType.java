package scv.pauls.measuresconverter;

/**
 * Created by Pauls on 12.01.2017.
 */

public class MeasureType {
    private String measureCode;
    private String measureDescr;

    public MeasureType(String measureCode, String measureDescr) {
        super();
        this.measureCode = measureCode;
        this.measureDescr = measureDescr;
    }
    public String getMeasureCode() {
        return measureCode;
    }
    public void setMeasureCode(String measureCode) {
        this.measureCode = measureCode;
    }
    public String getMeasureDescr() {
        return measureDescr;
    }
    public void setMeasureDescr(String measureDescr) {
        this.measureDescr = measureDescr;
    }
    @Override
    public String toString() {
        return "Measure [measureCode=" + measureCode + ", measureDescr=" + measureDescr + "]";
    }
}
