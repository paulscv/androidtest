package scv.pauls.measuresconverter;

import java.util.ArrayList;

public interface IMeasureSystem {
	public MeasureSystem getMeasureSystem(String systemCode);
	public ArrayList<MeasureSystem> getAllMeasureSystems();
	public ArrayList<String> getMeasureSystemNames();	
}
