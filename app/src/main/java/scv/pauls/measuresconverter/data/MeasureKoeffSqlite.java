package scv.pauls.measuresconverter.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;

import scv.pauls.measuresconverter.IMeasureKoeff;
import scv.pauls.measuresconverter.MeasureKoeff;
import scv.pauls.measuresconverter.MeasureName;

;

/**
 * Created by Pauls on 12.01.2017.
 */

public class MeasureKoeffSqlite implements IMeasureKoeff {

	private  final String LOG_TAG = "mcLog";
	private static final String selectKoef = "SELECT CODEFROM, CODETO, KOEF FROM MEASUREKOEF WHERE CODEFROM = ? AND CODETO = ?";
	private SQLiteDatabase db;
	// Зададим условие для выборки - список столбцов
	private String[] projection = {
			MeasuresDataContract.MeasureKoeffEntry.COLUMN_CODEFROM,
			MeasuresDataContract.MeasureKoeffEntry.COLUMN_CODETO,
			MeasuresDataContract.MeasureKoeffEntry.COLUMN_KOEFF};

	private String selectMeasureNameByCode = MeasuresDataContract.MeasureKoeffEntry.COLUMN_CODEFROM + "= ? AND "+ MeasuresDataContract.MeasureKoeffEntry.COLUMN_CODETO + "= ? ";
	
	public MeasureKoeffSqlite(SQLiteDatabase sqldb){
		super();
		this.db = sqldb;
	}
	
	@Override
	public MeasureKoeff getKoeff(String codeF, String codeT) {
	   	    MeasureKoeff res = null;
	   try {  
		    res = getKoefInside(codeF, codeT); // // пытаемся получить сразу
		   if (res == null){ //если не пошло, ищим через базовые коэффициенты
		    	res = getKoefInside(codeT, codeF); //  пытаемся получить в обратную сторону
			    if (res == null){ //если не пошло и так и так, ищем через базовые коэффициенты
			    	res = new MeasureKoeff(codeF, codeT);
			    	BigDecimal resKoef = new BigDecimal("1");
			    	resKoef.setScale(6, BigDecimal.ROUND_HALF_EVEN);
			    	
			    	MeasureKoeff baseKoefFrom = getBaseKoefForMeasureName(codeF); // Получили К между From и базовым измерение в его системе x
	
			    	MeasureKoeff baseKoefTo = getBaseKoefForMeasureName(codeT); // Получили К между To измерение и базовым в его системе z
	
			    	MeasureKoeff baseTobase = getKoefInside(baseKoefFrom.getCodeFrom(), baseKoefTo.getCodeFrom()); // Получили К между базовыми К в обеих системах	y
			    	
			    	if (baseTobase == null){ //Если не получили, пытаемся в другую сторону
			    		baseTobase = getKoefInside(baseKoefTo.getCodeFrom(), baseKoefFrom.getCodeFrom()); // Получили К между базовыми в обеих системах	y
						//K = z/(x*y)
				    	BigDecimal restmp = baseKoefTo.getKoeff().multiply(baseKoefFrom.getKoeff());
			    		resKoef = baseTobase.getKoeff().divide(restmp, 6, RoundingMode.HALF_UP);
			    	}
			    	else{
			      	   // K = (y*z)/x
				    	BigDecimal restmp = baseKoefTo.getKoeff().multiply(baseTobase.getKoeff());
				        resKoef = restmp.divide(baseKoefFrom.getKoeff(), 6, RoundingMode.HALF_UP);
			    	}
			    	res.setKoeff(resKoef);
			  }
			  else {
				  BigDecimal restmp =  new BigDecimal("1");			  
				  res.setKoeff(restmp.divide(res.getKoeff(), 6, RoundingMode.HALF_UP));
			  } 
		    } 
		} catch (SQLException | NullPointerException e) {
		   Log.e(LOG_TAG, "getKoeff. Error get right Koeff ");
				e.printStackTrace();						
		}
		return res;
	}

	
    private MeasureKoeff getKoefInside(String codeF, String codeT ) {
		MeasureKoeff res = null;
		String[] selectionArgs = {codeF, codeT} ;
		Cursor statement = null;
		try {
			// Делаем запрос
			statement = db.query(
					MeasuresDataContract.MeasureKoeffEntry.TABLE_NAME,   // таблица
					projection,            // столбцы
					selectMeasureNameByCode,             // столбцы для условия WHERE
					selectionArgs,         // значения для условия WHERE
					null,                  // Don't group the rows
					null,                  // Don't filter by row groups
					null);                 // порядок сортировки
			if (statement.getCount() > 0){
				statement.moveToFirst();
				res= new MeasureKoeff(codeF, codeT);
				res.setCodeFrom(statement.getString(statement.getColumnIndex(MeasuresDataContract.MeasureKoeffEntry.COLUMN_CODEFROM)));
				res.setCodeTo(statement.getString(statement.getColumnIndex(MeasuresDataContract.MeasureKoeffEntry.COLUMN_CODETO)));
				BigDecimal big = BigDecimal.valueOf(statement.getFloat(statement.getColumnIndex(MeasuresDataContract.MeasureKoeffEntry.COLUMN_KOEFF)));
				res.setKoeff(big);
			}
		} catch (SQLiteException e) {
			Log.e(LOG_TAG, "Error get Measure koeff  from" + codeF + " to "+codeT);
			e.printStackTrace();
		}
		finally	{
			if (statement != null) statement.close();
		}
		return res;  //если не нашли - null
    }
    
	private MeasureKoeff getBaseKoefForMeasureName (String code) throws SQLException{
		MeasureNameSqlite measuresName = new MeasureNameSqlite(db);
		MeasureName mName = measuresName.getMeasureName(code); //получили имя
		if (mName.isBaseMeasure()) 	//если уже базовая мера
		 return	new MeasureKoeff(code, code); //то сама  с собой дает 1 по-умолчанию
		else {	
		  MeasureName baseName = measuresName.getBaseMeasureName(mName.getMeasureSystemCode(), mName.getMeasureCode()); //иначе нашли базовую и получили К . Во From всегда базовый код
		  return getKoefInside(baseName.getCode(), mName.getCode()); 
		} 
	}
	
	
}
