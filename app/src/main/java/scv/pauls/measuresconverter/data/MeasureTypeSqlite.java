package scv.pauls.measuresconverter.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.util.ArrayList;

import scv.pauls.measuresconverter.IMeasureType;
import scv.pauls.measuresconverter.MeasureType;

/**
 * Created by Pauls on 12.01.2017.
 */
public class MeasureTypeSqlite implements IMeasureType {

	private static final String selectMeasureAll = "SELECT * FROM MEASURETYPES";
	private SQLiteDatabase db;
	// Зададим условие для выборки - список столбцов
	private String[] projection = {
			MeasuresDataContract.MeasureTypesEntry.COLUMN_CODE,
			MeasuresDataContract.MeasureTypesEntry.COLUMN_DESCR};
	private String selection = MeasuresDataContract.MeasureTypesEntry.COLUMN_CODE + "= ?";


	public MeasureTypeSqlite(SQLiteDatabase sqldb){
		super();
		this.db = sqldb;
	}
	
	@Override
	public MeasureType getMeasure(String measureCode) {
		String mCode = "", mDescr = "";
		String[] selectionArgs = {measureCode} ;
		Cursor statement = null;
		try {
			// Делаем запрос
			statement = db.query(
					MeasuresDataContract.MeasureTypesEntry.TABLE_NAME,   // таблица
					projection,            // столбцы
					selection,                  // столбцы для условия WHERE
					selectionArgs,                  // значения для условия WHERE
					null,                  // Don't group the rows
					null,                  // Don't filter by row groups
					null);                 // порядок сортировки
			if (statement.getCount() > 0){
			statement.moveToFirst();
				mCode = statement.getString(statement.getColumnIndex(MeasuresDataContract.MeasureSystemsEntry.COLUMN_CODE));
				mDescr = statement.getString(statement.getColumnIndex(MeasuresDataContract.MeasureSystemsEntry.COLUMN_DESCR));
			}		
		} catch (SQLiteException e) {
			System.out.println("Error get Measure " + measureCode);
			e.printStackTrace();
		}
		finally	{
			if (statement != null) statement.close();
		}
		return new MeasureType(mCode, mDescr);
	}

	@Override
	public ArrayList<MeasureType> getAllMeasures() {
		ArrayList<MeasureType> resList = new ArrayList<MeasureType>();
		String sysCode = "", sysDescr = "";
		Cursor statement = null;
		try  {
			statement = db.rawQuery(selectMeasureAll, null);
			statement.moveToFirst();
			while (!statement.isAfterLast()) {
				sysCode = statement.getString(statement.getColumnIndex(MeasuresDataContract.MeasureSystemsEntry.COLUMN_CODE));
				sysDescr =  statement.getString(statement.getColumnIndex(MeasuresDataContract.MeasureSystemsEntry.COLUMN_DESCR));
				resList.add(new MeasureType(sysCode, sysDescr));
				statement.moveToNext();
			}		
		} catch (SQLiteException e) {
			System.out.println("Error get Measures types list" );
 			e.printStackTrace();
		}
		finally	{if (statement != null) statement.close();
			}
		return resList;
	}

	@Override
	public ArrayList<String> getMeasureNames() {
		ArrayList<String> resList = new ArrayList<String>();
		String sysDescr = "";
		Cursor statement = null;
		try  {
			statement = db.rawQuery(selectMeasureAll, null);
			statement.moveToFirst();
			while (!statement.isAfterLast()) {
				sysDescr =  statement.getString(statement.getColumnIndex(MeasuresDataContract.MeasureSystemsEntry.COLUMN_DESCR));
				resList.add(sysDescr);
				statement.moveToNext();}
		} catch (SQLiteException e) {
			System.out.println("Error get Measures types descr list" );
			e.printStackTrace();
		}
		finally	{if (statement != null) statement.close();
		}
		return resList;
	}

}
