package scv.pauls.measuresconverter.data;

/**
 * Created by Pauls on 11.01.2017.
 */

public final class MeasuresDataContract {

    public MeasuresDataContract() {
    }

    public static class MeasureSystemsEntry{
        public final static String TABLE_NAME = "measuresystems";
        public final static String COLUMN_CODE = "CODE"; //имена полей должны быть большими буквами
        public final static String COLUMN_DESCR = "DESCR";
    }

    public static class MeasureTypesEntry{
        public final static String TABLE_NAME = "measuretypes";
        public final static String COLUMN_CODE = "CODE";
        public final static String COLUMN_DESCR = "DESCR";
    }

    public static class MeasureNamesEntry{
        public final static String TABLE_NAME = "measurenames";
        public final static String COLUMN_CODE = "CODE";
        public final static String COLUMN_DESCR = "DESCR";
        public final static String COLUMN_SYSCODE = "SYSCODE";
        public final static String COLUMN_MEASURECODE = "MEASURECODE";
        public final static String COLUMN_ISINSYS = "ISINSYS";
        public final static String COLUMN_ISBASE = "ISBASE";
    }

    public static class MeasureKoeffEntry{
        public final static String TABLE_NAME = "measurekoeff";
        public final static String COLUMN_CODEFROM = "CODEFROM";
        public final static String COLUMN_CODETO = "CODETO";
        public final static String COLUMN_KOEFF = "KOEFF";
    }

}
