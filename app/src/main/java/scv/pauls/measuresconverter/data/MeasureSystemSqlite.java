package scv.pauls.measuresconverter.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.util.ArrayList;

import scv.pauls.measuresconverter.IMeasureSystem;
import scv.pauls.measuresconverter.MeasureSystem;
/**
 * Created by Pauls on 12.01.2017.
 */

public class MeasureSystemSqlite implements IMeasureSystem {

	private static final String selectMeasureSystemAll = "SELECT * FROM MEASURESYSTEMS";
	private SQLiteDatabase db;
	// Зададим условие для выборки - список столбцов
	private String[] projection = {
			MeasuresDataContract.MeasureSystemsEntry.COLUMN_CODE,
			MeasuresDataContract.MeasureSystemsEntry.COLUMN_DESCR};
	private String selection = MeasuresDataContract.MeasureSystemsEntry.COLUMN_CODE + "= ?";

	
	public MeasureSystemSqlite(SQLiteDatabase sqldb){
		super();
		this.db = sqldb;
	}
	
	
	@Override
	public MeasureSystem getMeasureSystem(String systemCode) {
		String sysCode = "", sysDescr = "";
		String[] selectionArgs = {systemCode} ;
		Cursor statement = null;
		try {
			// Делаем запрос
			statement = db.query(
					MeasuresDataContract.MeasureSystemsEntry.TABLE_NAME,   // таблица
					projection,            // столбцы
					selection,                  // столбцы для условия WHERE
					selectionArgs,                  // значения для условия WHERE
					null,                  // Don't group the rows
					null,                  // Don't filter by row groups
					null);                 // порядок сортировки
			if (statement.getCount() > 0){
				statement.moveToFirst();
				sysCode = statement.getString(statement.getColumnIndex(MeasuresDataContract.MeasureSystemsEntry.COLUMN_CODE));
				sysDescr = statement.getString(statement.getColumnIndex(MeasuresDataContract.MeasureSystemsEntry.COLUMN_DESCR));
			}
		} catch (SQLiteException | IllegalStateException e) {
			System.out.println("Error get Measure system  from db " + systemCode);
			e.printStackTrace();
		}
		finally	{
			if (statement != null) statement.close();
		}
		return new MeasureSystem(sysCode, sysDescr);
	}

	@Override
	public ArrayList<MeasureSystem> getAllMeasureSystems() {
		ArrayList<MeasureSystem> resList = new ArrayList<MeasureSystem>();
		String sysCode = "", sysDescr = "";
		Cursor statement = null;
		try  {
			statement = db.rawQuery(selectMeasureSystemAll, null);
			statement.moveToFirst();
			//for ( String cn : statement.getColumnNames()) System.out.println(cn);
			while ( !statement.isAfterLast()) {
				sysCode = statement.getString(statement.getColumnIndex(MeasuresDataContract.MeasureSystemsEntry.COLUMN_CODE));
				sysDescr =  statement.getString(statement.getColumnIndex(MeasuresDataContract.MeasureSystemsEntry.COLUMN_DESCR));
				resList.add(new MeasureSystem(sysCode, sysDescr));
				statement.moveToNext();
			}
		} catch (SQLiteException | IllegalStateException e) {
			System.out.println("Error get Measures systems list" );
			e.printStackTrace();
		}
		finally	{if (statement != null) statement.close();
		}
		return resList;
	}

	@Override
	public ArrayList<String> getMeasureSystemNames() {
		ArrayList<String> resList = new ArrayList<String>();
		String sysDescr = "";

		Cursor statement = null;
		try  {
			statement = db.rawQuery(selectMeasureSystemAll, null);
			statement.moveToFirst();
			while (!statement.isAfterLast()) {
				sysDescr =  statement.getString(statement.getColumnIndex(MeasuresDataContract.MeasureSystemsEntry.COLUMN_DESCR));
				resList.add(sysDescr);
				statement.moveToNext();
			}
		} catch (SQLiteException | IllegalStateException e) {
			System.out.println("Error get Measures systems descr list" );
			e.printStackTrace();
		}
		finally	{if (statement != null) statement.close();
		}
		return resList;
	}

}
