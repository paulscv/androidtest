package scv.pauls.measuresconverter.data;

import android.content.Context;

import java.io.IOException;

/**
 * Created by Pauls on 11.01.2017.
 */

public class DBConnectionFactory   {
    private static Object dbInstance = null;

    public IDataConnection makeDBConnection( Object context, String dbType) throws  IOException {
        IDataConnection dbConn = null;
        if (dbType.equalsIgnoreCase("H2")) {
            // H2 connection
            //dbInstance = (DBConnectionH2) DBConnection.makeDataConnection((Shell) context);
            //dbConn = (IDataConnection) dbInstance;
        } else if (dbType.equalsIgnoreCase("SQLite")) {
            dbInstance = (SQLiteConnectionHelper) SQLiteConnectionHelper.makeDataConnection((Context) context);
            dbConn = (IDataConnection) dbInstance;
        } else if (dbType.equalsIgnoreCase("TXT")) {
            // TXT connection
        }
        return dbConn;
    }

    public static Object getDBInstanse() {return dbInstance;}
}
