package scv.pauls.measuresconverter.data;

/**
 * Created by Pauls on 11.01.2017.
 */

public interface IDataConnection<T> {
    public T getDataConnection();
}
