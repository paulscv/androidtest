package scv.pauls.measuresconverter.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.util.ArrayList;

import scv.pauls.measuresconverter.IMeasureName;
import scv.pauls.measuresconverter.MeasureName;
/**
 * Created by Pauls on 12.01.2017.
 */
public class MeasureNameSqlite implements IMeasureName {

	private static final String selectMeasureNamesAll = "SELECT * FROM MEASURENAMES";
	private SQLiteDatabase db;
	// Зададим условие для выборки - список столбцов
	private String[] projection = {
		MeasuresDataContract.MeasureNamesEntry.COLUMN_SYSCODE,
		MeasuresDataContract.MeasureNamesEntry.COLUMN_MEASURECODE,
		MeasuresDataContract.MeasureNamesEntry.COLUMN_CODE,
		MeasuresDataContract.MeasureNamesEntry.COLUMN_DESCR,
		MeasuresDataContract.MeasureNamesEntry.COLUMN_ISINSYS,
		MeasuresDataContract.MeasureNamesEntry.COLUMN_ISBASE};

	private String selectMeasureNameByCode = MeasuresDataContract.MeasureNamesEntry.COLUMN_CODE + " = ?";
	private String selectMeasureNamebySysCode = MeasuresDataContract.MeasureNamesEntry.COLUMN_SYSCODE + "= ?";
	private String selectMeasureNamebyMeasureCode = MeasuresDataContract.MeasureNamesEntry.COLUMN_MEASURECODE + "= ?";
	private String selectMeasureNameByBothCodes = MeasuresDataContract.MeasureNamesEntry.COLUMN_SYSCODE + "= ? AND "+MeasuresDataContract.MeasureNamesEntry.COLUMN_MEASURECODE +" = ?";
	private String selectMeasureNameBase = MeasuresDataContract.MeasureNamesEntry.COLUMN_SYSCODE + "= ? AND "+MeasuresDataContract.MeasureNamesEntry.COLUMN_MEASURECODE +" = ? AND ISBASE";

	private int sysCodeIdx, measureCodeIdx, codeNameidx,  descrIdx , isSysIdx , isBaseIdx ;

	public MeasureNameSqlite(SQLiteDatabase sqldb){
		super();
		this.db = sqldb;
	}

	private void getCoumnIdx(Cursor statement){
		 sysCodeIdx = statement.getColumnIndex(MeasuresDataContract.MeasureNamesEntry.COLUMN_SYSCODE);
		 measureCodeIdx = statement.getColumnIndex(MeasuresDataContract.MeasureNamesEntry.COLUMN_MEASURECODE);
		 codeNameidx = statement.getColumnIndex(MeasuresDataContract.MeasureNamesEntry.COLUMN_CODE);
		 descrIdx = statement.getColumnIndex(MeasuresDataContract.MeasureNamesEntry.COLUMN_DESCR);
		 isSysIdx = statement.getColumnIndex(MeasuresDataContract.MeasureNamesEntry.COLUMN_ISINSYS);
		 isBaseIdx = statement.getColumnIndex(MeasuresDataContract.MeasureNamesEntry.COLUMN_ISBASE);
	}


	@Override
	public MeasureName getMeasureName(String code) {
		String sysCode = "", mCode = "", codeName = "", descr = ""; 
		boolean isSys = false, isBase = false; 
		MeasureName res;
		Cursor statement = null;
		String[] selectionArgs = {code} ;

		try  {
			// Делаем запрос
			statement = db.query(
					MeasuresDataContract.MeasureNamesEntry.TABLE_NAME,   // таблица
					projection,            // столбцы
					selectMeasureNameByCode,  // столбцы для условия WHERE
					selectionArgs,            // значения для условия WHERE
					null,                  // Don't group the rows
					null,                  // Don't filter by row groups
					null);                 // порядок сортировки
			getCoumnIdx(statement);
			if (statement.getCount() > 0){
				statement.moveToFirst();
				sysCode = statement.getString(sysCodeIdx);
				mCode = statement.getString(measureCodeIdx);
				codeName = statement.getString(codeNameidx);
				descr = statement.getString(descrIdx);
				isSys = Boolean.valueOf(statement.getString(isSysIdx).equalsIgnoreCase("1") ?"true" : "false");
				isBase = Boolean.valueOf(statement.getString(isBaseIdx).equalsIgnoreCase("1") ?"true" : "false");
				MeasureName m = new MeasureName(sysCode, mCode, codeName, descr);
				m.setInSystem(isSys);
				m.setIsBaseMeasure(isBase);
			}
		} catch (SQLiteException e) {
			System.out.println("Error get Measure Name " + code);
			e.printStackTrace();
		}
		 finally {
			res  = new MeasureName(sysCode, mCode, codeName, descr);
			res.setInSystem(isSys);
			res.setIsBaseMeasure(isBase);
		}		
		return res;
	}

	@Override
	public ArrayList<MeasureName> getAllMeasureNames() {
		ArrayList<MeasureName> resList = new ArrayList<MeasureName>();
		String sysCode = "", measureCode = "", codeName = "", descr = ""; 
		boolean isSys = false, isBase = false;
		Cursor statement = null;
		try  {
			statement = db.rawQuery(selectMeasureNamesAll, null);
			getCoumnIdx(statement);
			statement.moveToFirst();
			while (!statement.isAfterLast() ) {
				sysCode = statement.getString(sysCodeIdx);
				measureCode = statement.getString(measureCodeIdx);
				codeName = statement.getString(codeNameidx);
				descr = statement.getString(descrIdx);
				isSys = Boolean.valueOf(statement.getString(isSysIdx).equalsIgnoreCase("1") ?"true" : "false");
				isBase = Boolean.valueOf(statement.getString(isBaseIdx).equalsIgnoreCase("1") ?"true" : "false");
				MeasureName m = new MeasureName(sysCode, measureCode, codeName, descr);
				m.setInSystem(isSys);
				m.setIsBaseMeasure(isBase);
				resList.add(m);
				statement.moveToNext();
			}
		} catch (SQLiteException e) {
			System.out.println("Error get Measures names list" );
			e.printStackTrace();
		}
		finally	{if (statement != null) statement.close();
		}
		return resList;
	}

	@Override
	public ArrayList<MeasureName> getMeasureNamesBySystem(String systemCode) {
		ArrayList<MeasureName> resList = new ArrayList<MeasureName>();
		String sysCode = "", mCode = "", codeName = "", descr = ""; 
		boolean isSys = false, isBase = false;

		Cursor statement = null;
		String[] selectionArgs = {systemCode} ;

		try  {
			// Делаем запрос
			statement = db.query(
					MeasuresDataContract.MeasureNamesEntry.TABLE_NAME,   // таблица
					projection,            // столбцы
					selectMeasureNamebySysCode,  // столбцы для условия WHERE
					selectionArgs,            // значения для условия WHERE
					null,                  // Don't group the rows
					null,                  // Don't filter by row groups
					null);                 // порядок сортировки
			getCoumnIdx(statement);
			statement.moveToFirst();
			while (!statement.isAfterLast()) {
				sysCode = statement.getString(sysCodeIdx);
				mCode = statement.getString(measureCodeIdx);
				codeName = statement.getString(codeNameidx);
				descr = statement.getString(descrIdx);
				isSys = Boolean.valueOf(statement.getString(isSysIdx).equalsIgnoreCase("1") ?"true" : "false");
				isBase = Boolean.valueOf(statement.getString(isBaseIdx).equalsIgnoreCase("1") ?"true" : "false");
				MeasureName m = new MeasureName(sysCode, mCode, codeName, descr);
				m.setInSystem(isSys);
				m.setIsBaseMeasure(isBase);
				resList.add(m);
				statement.moveToNext();
			}
		} catch (SQLiteException e) {
			System.out.println("Error get Measures names list " + systemCode);
			e.printStackTrace();
		}
		finally {if (statement != null) statement.close();
		}
		return resList;
	}

	@Override
	public ArrayList<MeasureName> getMeasureNamesByType(String measureCode) {
		ArrayList<MeasureName> resList = new ArrayList<MeasureName>();
		String sysCode = "", mCode = "", codeName = "", descr = ""; 
		boolean isSys = false, isBase = false;

		Cursor statement = null;
		String[] selectionArgs = {measureCode} ;

		try  {
			// Делаем запрос
			statement = db.query(
					MeasuresDataContract.MeasureNamesEntry.TABLE_NAME,   // таблица
					projection,            // столбцы
					selectMeasureNamebyMeasureCode,  // столбцы для условия WHERE
					selectionArgs,            // значения для условия WHERE
					null,                  // Don't group the rows
					null,                  // Don't filter by row groups
					null);                 // порядок сортировки
			getCoumnIdx(statement);
			statement.moveToFirst();
			while (!statement.isAfterLast()) {
				sysCode = statement.getString(sysCodeIdx);
				mCode = statement.getString(measureCodeIdx);
				codeName = statement.getString(codeNameidx);
				descr = statement.getString(descrIdx);
				isSys = Boolean.valueOf(statement.getString(isSysIdx).equalsIgnoreCase("1") ?"true" : "false");
				isBase = Boolean.valueOf(statement.getString(isBaseIdx).equalsIgnoreCase("1") ?"true" : "false");
				MeasureName m = new MeasureName(sysCode, mCode, codeName, descr);
				m.setInSystem(isSys);
				m.setIsBaseMeasure(isBase);
				resList.add(m);
				statement.moveToNext();
			}
		} catch (SQLiteException e) {
			System.out.println("Error get Measures names list " + selectMeasureNamebyMeasureCode);
			e.printStackTrace();
		}
		finally {if (statement != null) statement.close();
		}
		return resList;
	}

	@Override
	public ArrayList<MeasureName> getMeasureNamesBySystemAndType(String systemCode, String measureCode) {
		ArrayList<MeasureName> resList = new ArrayList<MeasureName>();
		String sysCode = "", mCode = "", codeName = "", descr = ""; 
		boolean isSys = false, isBase = false;
		Cursor statement = null;
		String[] selectionArgs = {systemCode, measureCode} ;

		try  {
			// Делаем запрос
			statement = db.query(
					MeasuresDataContract.MeasureNamesEntry.TABLE_NAME,   // таблица
					projection,            // столбцы
					selectMeasureNameByBothCodes,  // столбцы для условия WHERE
					selectionArgs,            // значения для условия WHERE
					null,                  // Don't group the rows
					null,                  // Don't filter by row groups
					null);                 // порядок сортировки
			getCoumnIdx(statement);
			statement.moveToFirst();
			while (!statement.isAfterLast()) {
				sysCode = statement.getString(sysCodeIdx);
				mCode = statement.getString(measureCodeIdx);
				codeName = statement.getString(codeNameidx);
				descr = statement.getString(descrIdx);
				isSys = Boolean.valueOf(statement.getString(isSysIdx).equalsIgnoreCase("1") ?"true" : "false");
				isBase = Boolean.valueOf(statement.getString(isBaseIdx).equalsIgnoreCase("1") ?"true" : "false");
				MeasureName m = new MeasureName(sysCode, mCode, codeName, descr);
				m.setInSystem(isSys);
				m.setIsBaseMeasure(isBase);
				resList.add(m);
				statement.moveToNext();
			}
		} catch (SQLiteException e) {
			System.out.println("Error get Measures names list" + selectMeasureNamebyMeasureCode);
			e.printStackTrace();
		}
		finally {if (statement != null) statement.close();
		}
		return resList;
	}

	@Override
	public MeasureName getBaseMeasureName(String systemCode, String measureCode) {
		String sysCode = "", mCode = "", codeName = "", descr = ""; 
		boolean isSys = false, isBase = false; 
		MeasureName res;
		String[] selectionArgs = {systemCode, measureCode} ;
		Cursor statement = null;

		try  {
			// Делаем запрос
			statement = db.query(
					MeasuresDataContract.MeasureNamesEntry.TABLE_NAME,   // таблица
					projection,            // столбцы
					selectMeasureNameBase,  // столбцы для условия WHERE
					selectionArgs,            // значения для условия WHERE
					null,                  // Don't group the rows
					null,                  // Don't filter by row groups
					null);                 // порядок сортировки
			getCoumnIdx(statement);
			if (statement.getCount() > 0){
				statement.moveToFirst();
				sysCode = statement.getString(sysCodeIdx);
				mCode = statement.getString(measureCodeIdx);
				codeName = statement.getString(codeNameidx);
				descr = statement.getString(descrIdx);
				isSys = Boolean.valueOf(statement.getString(isSysIdx).equalsIgnoreCase("1") ?"true" : "false");
				isBase = Boolean.valueOf(statement.getString(isBaseIdx).equalsIgnoreCase("1") ?"true" : "false");
				res = new MeasureName(sysCode, mCode, codeName, descr);
				res.setInSystem(isSys);
				res.setIsBaseMeasure(isBase);
			}
		} catch (SQLiteException e) {
			System.out.println("Error get Measures name " + selectMeasureNamebyMeasureCode);
			e.printStackTrace();
		}
		finally {if (statement != null) statement.close();
			res = new MeasureName(sysCode, mCode, codeName, descr);
			res.setInSystem(isSys);
			res.setIsBaseMeasure(isBase);
		}
		return res;
	}

}
