package scv.pauls.measuresconverter.data;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * Created by Pauls on 11.01.2017.
 */

public class SQLiteConnectionHelper extends SQLiteOpenHelper implements IDataConnection<SQLiteDatabase> {



    // путь к базе данных вашего приложения
    private static final String LOG_TAG = "mcLog";
    private static String DB_PATH = "/data/data/scv.pauls.measuresconverter/databases/";
    private static String DB_NAME = "measures.db";
    private  SQLiteDatabase myDataBase;
    private  static SQLiteConnectionHelper dbInstance = null;
    private final Context mContext;
   // private static final int DATABASE_VERSION = 1; //ToDo сделать версинность базы

    /**
     * Конструктор
     * Принимает и сохраняет ссылку на переданный контекст для доступа к ресурсам приложения
     * @param context
     */
    private SQLiteConnectionHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.mContext = context;
    }

    public static SQLiteConnectionHelper makeDataConnection(Context context) {
        if (dbInstance == null){
            dbInstance =  new SQLiteConnectionHelper(context);
            try {
                dbInstance.createDataBase();
                dbInstance.openDataBase();
            }
            catch (IOException ex){
                ex.printStackTrace();
            }
        }
        return dbInstance;
    }

    @Override
    public SQLiteDatabase getDataConnection() {
        return myDataBase;
    }


    /**
     * Создает пустую базу данных и перезаписывает ее нашей собственной базой
     * */
    private void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();

        if(dbExist){
            //ничего не делать - база уже есть
            Log.d(LOG_TAG, "DB "+ DB_NAME +" allready exists");
        }else{
            //вызывая этот метод создаем пустую базу, позже она будет перезаписана
            this.getReadableDatabase();

            try {
                copyDataBase();
            } catch (IOException e) {
                Log.e(LOG_TAG, "Error copying database " + DB_PATH+DB_NAME);
                throw new Error("Error copying database " +DB_PATH+DB_NAME);
            }
        }
    }

    /**
     * Проверяет, существует ли уже эта база, чтобы не копировать каждый раз при запуске приложения
     * @return true если существует, false если не существует
     */
    private boolean checkDataBase(){
        SQLiteDatabase checkDB = null;

        try{
            String myPath = DB_PATH + DB_NAME;
            Log.d(LOG_TAG, "Trying open DB " +DB_NAME);
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        }catch(SQLiteException e){
            //база еще не существует
            Log.w(LOG_TAG, "DB "+ DB_NAME +" does not exists");
        }
        if(checkDB != null){
            checkDB.close();
        }
        return false;//checkDB != null ? true : false;
    }

    /**
     * Копирует базу из папки assets заместо созданной локальной БД
     * Выполняется путем копирования потока байтов.
     * */
    private void copyDataBase() throws IOException{
        Log.d(LOG_TAG, "Copying DB to " +DB_PATH + DB_NAME);
        //Открываем локальную БД как входящий поток
        InputStream myInput = mContext.getAssets().open(DB_NAME);

        //Путь ко вновь созданной БД
        String outFileName = DB_PATH + DB_NAME;

        //Открываем пустую базу данных как исходящий поток
        OutputStream myOutput = new FileOutputStream(outFileName);

        //перемещаем байты из входящего файла в исходящий
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        //закрываем потоки
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    private void openDataBase() throws SQLException {
        //открываем БД
        Log.d(LOG_TAG, "Opening Existing DB" +DB_PATH + DB_NAME);
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    }

    @Override
    public synchronized void close() {
        Log.d(LOG_TAG, "Close DB " + DB_NAME);
        if(myDataBase != null)
            myDataBase.close();
        super.close();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
