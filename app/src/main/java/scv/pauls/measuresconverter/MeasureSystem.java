package scv.pauls.measuresconverter;

/**
 * Created by Pauls on 12.01.2017.
 */

public class MeasureSystem {
    private String measureSystemCode;
    private String masureSystemDescr;

    public MeasureSystem(String measureSystemCode, String masureSystemDescr) {
        super();
        this.measureSystemCode = measureSystemCode;
        this.masureSystemDescr = masureSystemDescr;
    }

    public String getMeasureSystemCode() {
        return measureSystemCode;
    }
    public void setMeasureSystemCode(String measureSystemCode) {
        this.measureSystemCode = measureSystemCode;
    }
    public String getMasureSystemDescr() {
        return masureSystemDescr;
    }
    public void setMasureSystemDescr(String masureSystemDescr) {
        this.masureSystemDescr = masureSystemDescr;
    }

    @Override
    public String toString() {
        return "MeasureSystem [" + measureSystemCode + ", " + masureSystemDescr + "]";
    }

}
