package scv.pauls.measuresconverter;

import java.util.ArrayList;

public interface IMeasureName {
	public MeasureName getMeasureName(String code);
	public ArrayList<MeasureName> getAllMeasureNames();
	public ArrayList<MeasureName> getMeasureNamesBySystem(String systemCode);
	public ArrayList<MeasureName> getMeasureNamesByType(String measureCode);
	public ArrayList<MeasureName> getMeasureNamesBySystemAndType(String systemCode, String measureCode);
	public MeasureName getBaseMeasureName(String systemCode, String measureCode);
}
