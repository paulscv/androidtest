package scv.pauls.measuresconverter;

import java.math.BigDecimal;

/**
 * Created by Pauls on 12.01.2017.
 */

public class MeasureKoeff {
    private String codeFrom;
    private String codeTo;
    private BigDecimal koef;


    public MeasureKoeff(String codeFrom, String codeTo) {
        super();
        this.codeFrom = codeFrom;
        this.codeTo = codeTo;
        this.koef = new BigDecimal("1.00");
        this.koef.setScale(6, BigDecimal.ROUND_HALF_EVEN);
    }


    public String getCodeFrom() {
        return codeFrom;
    }

    public void setCodeFrom(String codeFrom) {
        this.codeFrom = codeFrom;
    }

    public String getCodeTo() {
        return codeTo;
    }

    public void setCodeTo(String codeTo) {
        this.codeTo = codeTo;
    }

    public BigDecimal getKoeff() {
        return koef;
    }

    public void setKoeff(BigDecimal koef) {
        this.koef = koef;
    }

    @Override
    public String toString() {
        return "MeasureKoef [codeFrom=" + codeFrom + ", codeTo=" + codeTo + ", koef=" + koef + "]";
    }

}
