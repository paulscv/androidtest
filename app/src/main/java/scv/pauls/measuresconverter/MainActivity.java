package scv.pauls.measuresconverter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

import scv.pauls.measuresconverter.data.DBConnectionFactory;
import scv.pauls.measuresconverter.data.IDataConnection;
import scv.pauls.measuresconverter.data.MeasureKoeffSqlite;
import scv.pauls.measuresconverter.data.MeasureNameSqlite;
import scv.pauls.measuresconverter.data.MeasureSystemSqlite;
import scv.pauls.measuresconverter.data.MeasureTypeSqlite;
import scv.pauls.measuresconverter.data.SQLiteConnectionHelper;

public class MainActivity extends AppCompatActivity {

   private  final String LOG_TAG = "mcLog";

    private  String txtFrom;
    private  String txtTo;

    private  String systemToSelected = "0";
    private  String systemFromSelected = "0";
    private  String measureTypeSelected = "0";

    private  ArrayList<MeasureSystem> arrListSystems;
    private  ArrayList<MeasureType> arrListMeasureTypes;
    private  ArrayList<MeasureName> mFrom;
    private  ArrayList<MeasureName> mTo;

    private  IMeasureSystem measureSystems;
    private  IMeasureType measureTypes;
    private  IMeasureName measureNamesFrom;
    private  IMeasureName measureNamesTo;

    private  Spinner measureTypesSpinner;
    private  Spinner  measureSystemFromSpinner;
    private  Spinner  measureNamesFromSpinner;
    private  Spinner  measureSystemToSpinner;
    private  Spinner  measureNamesToSpinner;

    private  volatile ArrayAdapter<String> adapterMeasureNamesFrom;
    private  volatile ArrayAdapter<String> adapterMeasureNamesTo;

    private  EditText editFrom;
    private  EditText editTo;
    private Button btnConvert;

    private  SQLiteDatabase dbConn;
    DBConnectionFactory dcf = new DBConnectionFactory();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(LOG_TAG, "Создаем основной Instance приложения");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(LOG_TAG, "Создаем подключение к БД");

        try {

            IDataConnection iConn = dcf.makeDBConnection(this,"SQLite");
            dbConn =  (SQLiteDatabase) iConn.getDataConnection();

            Log.d(LOG_TAG, "Добавляем перечень систем измерений в список");
            measureSystems = new MeasureSystemSqlite(dbConn);
            arrListSystems = measureSystems.getAllMeasureSystems();
            String[] arrSystems = new String[arrListSystems.size() + 1];
            arrSystems[0] = "ВСЕ";
            for (int i = 0; i < arrListSystems.size(); i++)   arrSystems[i + 1] = arrListSystems.get(i).getMasureSystemDescr();
            ArrayAdapter<String> adapterMeasureSystemsFrom = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrSystems);
            adapterMeasureSystemsFrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); //устанавливаем тип списка в "выпадающий"
            measureSystemFromSpinner = (Spinner) findViewById(R.id.measure_system_from_spinner);
            measureSystemFromSpinner.setAdapter(adapterMeasureSystemsFrom);
            measureSystemFromSpinner.setPrompt("Системы мер");        // заголовок
            measureSystemFromSpinner.setSelection(0);        // выделяем 1-й элемент

            Log.d(LOG_TAG, "Добавляем перечень типов измерений в список");
            measureTypes = new MeasureTypeSqlite(dbConn);
            arrListMeasureTypes = measureTypes.getAllMeasures();
            String[] mesuresTypesArr = new String[arrListMeasureTypes.size() + 1];
            mesuresTypesArr[0] = "ВСЕ";
            for (int i = 0; i < arrListMeasureTypes.size(); i++) mesuresTypesArr[i + 1] = arrListMeasureTypes.get(i).getMeasureDescr();
            ArrayAdapter<String> adapterMeasureTypes = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mesuresTypesArr);
            adapterMeasureTypes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            measureTypesSpinner = (Spinner) findViewById(R.id.measure_type_spinner);
            measureTypesSpinner.setAdapter(adapterMeasureTypes);
            measureTypesSpinner.setPrompt("Типы измерений");        // заголовок
            measureTypesSpinner.setSelection(0);        // выделяем 1-й элемент

            Log.d(LOG_TAG, "Добавляем  первоначальный перечень наименований измерений, из которых будем конвертить в список");
            measureNamesFrom = new MeasureNameSqlite(dbConn);
            mFrom = makeMeasureNamesArr(measureNamesFrom, systemFromSelected, measureTypeSelected);
            ArrayList<String> strNamesArrFrom = new ArrayList<String>();
            for (MeasureName mm : mFrom)  strNamesArrFrom.add(mm.getCodeDescr());
            adapterMeasureNamesFrom = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, strNamesArrFrom);
            adapterMeasureNamesFrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            adapterMeasureNamesFrom.setNotifyOnChange(true);
            measureNamesFromSpinner = (Spinner) findViewById(R.id.measure_name_from_spinner);
            measureNamesFromSpinner.setAdapter(adapterMeasureNamesFrom);
            measureNamesFromSpinner.setPrompt("Наименование измерений");        // заголовок
            measureNamesFromSpinner.setSelection(0);        // выделяем элемент

            Log.d(LOG_TAG, "Добавляем перечень систем измерений, в которые будем конвертировать");
            ArrayAdapter<String> adapterMeasureSystemsTo = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrSystems);
            adapterMeasureSystemsTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); //устанавливаем тип списка в "выпадающий"
            measureSystemToSpinner = (Spinner) findViewById(R.id.measure_system_to_spinner);
            measureSystemToSpinner.setAdapter(adapterMeasureSystemsTo);
            measureSystemToSpinner.setPrompt("Системы мер");        // заголовок
            measureSystemToSpinner.setSelection(0);


            Log.d(LOG_TAG, "Добавляем первоначальный список наименований мер, в которую будем конвертировать");
            measureNamesTo = new MeasureNameSqlite(dbConn);
            mTo = makeMeasureNamesArr(measureNamesTo, systemToSelected, measureTypeSelected);
            ArrayList<String> strNamesArrTo = new ArrayList<String>();
            for (MeasureName mm : mTo) strNamesArrTo.add(mm.getCodeDescr());
            adapterMeasureNamesTo = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, strNamesArrTo);
            adapterMeasureNamesTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            measureNamesToSpinner = (Spinner) findViewById(R.id.measure_name_to_spinner);
            measureNamesToSpinner.setAdapter(adapterMeasureNamesTo);
            measureNamesToSpinner.setPrompt("Наименования измерений");
            measureNamesToSpinner.setSelection(0);

            measureSystemFromSpinner.setOnItemSelectedListener(new SystemMeasureFromSelection());        //обработчик выбора
            measureTypesSpinner.setOnItemSelectedListener(new MeasureTypesSelection());        //обработчик выбора
            measureSystemToSpinner.setOnItemSelectedListener(new SystemMeasureToSelection());

            editFrom = (EditText) findViewById(R.id.editFrom);
            editFrom.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN &&
                            (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        //передаем фокус на кнопку
                        btnConvert.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        return true;
                    }
                    return false;
                }
            });
            editTo = (EditText) findViewById(R.id.editTo);
            editTo.setEnabled(false);
            editTo.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN &&
                            (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        //пока ничего не делаем
                    }
                    return false;
                }
            });

            btnConvert = (Button) findViewById(R.id.btnConvert);
            btnConvert.setOnClickListener(new ButtonConvertClick());
        }
        catch (IOException e) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Cann`t create and open DB! ",
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy(){
        ((SQLiteConnectionHelper) dcf.getDBInstanse()).close();
        Log.d(LOG_TAG, "Closing aplication measureconverter");
        super.onDestroy();
    }

    private class ButtonConvertClick implements AdapterView.OnClickListener{
        @Override
        public void onClick(View v) {
            v.post(new Runnable() {
                @Override
                public void run() {
                    Log.d(LOG_TAG,"ButtonConvertClick. Получаем коэффициент конвертации");
                    MeasureKoeffSqlite mKoefDb = new MeasureKoeffSqlite(dbConn);
                    String codeFrom = mFrom.get(measureNamesFromSpinner.getSelectedItemPosition()).getCode();
                    String codeTo = mTo.get(measureNamesToSpinner.getSelectedItemPosition()).getCode();
                    Log.d(LOG_TAG,"ButtonConvertClick. Из кода "+codeFrom+" в код "+codeTo);
                    MeasureKoeff mKoef;
                    mKoef = mKoefDb.getKoeff(codeFrom, codeTo);
                    Log.d(LOG_TAG,"ButtonConvertClick. значение коеффициента " + mKoef.getKoeff().toString());
                    if (mKoef != null) {
                        try {
                            BigDecimal koef = mKoef.getKoeff();
                            BigDecimal bdFrom = new BigDecimal((editFrom.getText().toString() == "") ? "0" : editFrom.getText().toString());
                            BigDecimal convRes = bdFrom.multiply(koef);
                            editTo.setText(convRes.toString());
                        }
                        catch (NumberFormatException ex){
                            Log.e(LOG_TAG, "ButtonConvertClick. Cann`t get result! NumberFormatException!"+ editFrom.getText().toString());
                        }
                    } else {
                        //создаем и отображаем текстовое уведомление
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Cann`t get result! Check DB connection!",
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        Log.e(LOG_TAG, "ButtonConvertClick. Cann`t get result! Check DB connection!");
                    }
                }
            });
        }
    }

    private class SystemMeasureFromSelection implements AdapterView.OnItemSelectedListener {
        public   void  onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0) systemFromSelected = "0"; // "Все" подменяем на "0".
            else systemFromSelected = arrListSystems.get(position - 1).getMeasureSystemCode();
            Log.d(LOG_TAG, "SystemMeasureFromSelection. Заполняем Spinner наименования мер новыми значениями");
            view.post(new Runnable() {
                          @Override
                          public void run() {
                              mFrom = makeMeasureNamesArr(measureNamesFrom, systemFromSelected, measureTypeSelected);
                              ArrayList<String> strNamesArrFrom = new ArrayList<String>();
                              for (MeasureName mm : mFrom) {
                                  strNamesArrFrom.add(mm.getCodeDescr());
                              }

                              adapterMeasureNamesFrom.clear();
                               adapterMeasureNamesFrom.addAll(strNamesArrFrom);

                              adapterMeasureNamesFrom.notifyDataSetChanged();
                          }
                      });
        }
        public  void  onNothingSelected(AdapterView<?> parent){
            // TODO Auto-generated method stub
        }
    }

    private class MeasureTypesSelection implements AdapterView.OnItemSelectedListener {
        @Override
        public   void  onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0) measureTypeSelected = "0"; // "Все" подменяем на "0".
            else measureTypeSelected = arrListMeasureTypes.get(position - 1).getMeasureCode();
            Log.d(LOG_TAG, "MeasureTypesSelection. Заполняем Spinner From наименования мер новыми значениями");
            view.post(new Runnable() {
                @Override
                public void run() {
                    mFrom = makeMeasureNamesArr(measureNamesFrom, systemFromSelected, measureTypeSelected);
                    ArrayList<String> strNamesArrFrom = new ArrayList<String>();
                    for (MeasureName mm : mFrom) strNamesArrFrom.add(mm.getCodeDescr());
                    adapterMeasureNamesFrom.clear();
                    adapterMeasureNamesFrom.addAll(strNamesArrFrom);
                    adapterMeasureNamesFrom.notifyDataSetChanged();

                    //Заполняем Spinner To наименования мер новыми значениями
                    mTo = makeMeasureNamesArr(measureNamesFrom, systemFromSelected, measureTypeSelected);
                    ArrayList<String>  strNamesArrTo = new ArrayList<String>();
                    for (MeasureName mm : mTo) strNamesArrTo.add(mm.getCodeDescr());
                    adapterMeasureNamesTo.clear();
                    adapterMeasureNamesTo.addAll(strNamesArrTo);
                    adapterMeasureNamesTo.notifyDataSetChanged();
                }
            });
        }
        public  void  onNothingSelected(AdapterView<?> parent){
            // TODO Auto-generated method stub
        }
    }

    private class SystemMeasureToSelection implements AdapterView.OnItemSelectedListener {
        @Override
        public   void  onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0) systemToSelected = "0"; // "Все" подменяем на "0".
            else systemToSelected = arrListSystems.get(position - 1).getMeasureSystemCode();
            Log.d(LOG_TAG, "SystemMeasureToSelection. Заполняем Spinner наименования мер новыми значениями");
            view.post(new Runnable() {
                @Override
                public void run() {
                    mTo = makeMeasureNamesArr(measureNamesTo, systemToSelected, measureTypeSelected);
                    ArrayList<String>  strNamesArrTo  = new ArrayList<String>();
                    for (MeasureName mm : mTo) strNamesArrTo.add(mm.getCodeDescr());
                    adapterMeasureNamesTo.clear();
                    adapterMeasureNamesTo.addAll(strNamesArrTo);
                    adapterMeasureNamesTo.notifyDataSetChanged();
                }
            });
        }
        public  void  onNothingSelected(AdapterView<?> parent){
            // TODO Auto-generated method stub
        }
    }

    private ArrayList<MeasureName> makeMeasureNamesArr(IMeasureName mn, String sysCode, String mCode){
        ArrayList<MeasureName> res;
        Log.d(LOG_TAG, "makeMeasureNamesArr. Формируем список наименовай мер в зависимости от выбора систем и типов измерений");
        Log.d(LOG_TAG, "makeMeasureNamesArr. Получаем наименования мер для sysCode " + sysCode + " mCode " + mCode);
        if ((sysCode.compareTo("0") != 0 ) && (mCode.compareTo("0") != 0 )) {	// Т.е. не "ВСЕ"
            res = mn.getMeasureNamesBySystemAndType(sysCode, mCode);
        }
        else if ((sysCode.compareTo("0") != 0 ) && (mCode.compareTo("0") == 0 )) {
            res = mn.getMeasureNamesBySystem(sysCode);
        }
        else if ((sysCode.compareTo("0") == 0 ) && (mCode.compareTo("0") != 0 )){
            res = mn.getMeasureNamesByType(mCode);
        }
        else res = mn.getAllMeasureNames();
        Log.d(LOG_TAG, "makeMeasureNamesArr. Количество наименований мер отобрано " + res.size() );
        return res;
    }

}
