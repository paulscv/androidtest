package scv.pauls.measuresconverter;

import java.math.BigDecimal;
import java.sql.SQLException;

public interface IMeasureKoeff {
	public MeasureKoeff getKoeff(String codeFrom, String codeTo) ;
}
